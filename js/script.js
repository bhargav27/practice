(function() {

  // menu toggle

    var menu = document.querySelector('.nav-links'),
    menulink = document.querySelector('#nav-toggle');
 
     menulink.addEventListener('click', function(e){
     menu.classList.toggle('active');     
     e.preventDefault();
   });

})();


// trusted slider

$(document).on('ready', function() {
    $(".trustedslider").slick({
        dots: false,
        arrows: false,
        autoplay: true,
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        centerMode: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    centerMode: true,
                    dots: true
                }                    
            }
        ]
    });   
});

$(document).on('ready', function() {
    $(".reviewslider").slick({
        dots: false,
        arrows: false,
        autoplay: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: true,
        responsive: [
            {
              breakpoint: 991,
              settings: {
                    slidesToShow: 2,
                    dots: false,
                    centerMode: true
              }
            },
            {
              breakpoint: 700,
              settings: {
                arrows: false,
                dots: false,
                centerMode: true,
                slidesToShow: 1
              }
            }
          ]
    });
}); 


$(document).on('ready', function() {
    
    if (window.matchMedia("(max-width: 991px)").matches) {
        $(".connectorsslider").slick({
            dots: true,
            arrows: false,
            autoplay: true,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            centerMode: true
        });
      } 
});
    

$('.tab').on('click', function (e) {
    e.preventDefault();
  $('.tab, .panel').removeClass('active');
  $(this).add('#' + $(this).attr('id').replace(/\s*tab\s*/, 'panel')).addClass('active');
  $(this).focus();
});


//*********************************************** */ user slider start ****************************
$(document).on('ready', function() {
    $(".usersslider").slick({
        dots: true,
        arrows: false,
        autoplay: true,
        infinite: true,
        slidesToShow: 1,
        variableWidth: true,
        centerMode: true,
        slidesToScroll: 1,
        responsive: [
            {
              breakpoint: 991,
              settings: {
                    slidesToShow: 1,
                    variableWidth: true,
                    dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                arrows: false,
                dots: true,
                slidesToShow: 1
              }
            }
          ]
    });
});
//*********************************************** */ user slider end ******************************